$( document ).ready(function() {
    getPacientesCGI()
})


let getPacientesCGI = () => {
    $.ajax({
        type:"POST",
        url: "/getPacientesCGI",
        success:function(response){
            crearTablaCGI(response)
        },
        error:function(response){
        }
    })
}

let crearTablaCGI = (data) => {
    let tbl = $("#tablaCGI");
    if(data == ""){
        empty_row(tbl[0].id,'No hay datos en esta tabla')
        return
    }
    
    $("#tablaCGI tbody").empty();
    let tabla = $("#tablaCGI tbody");
    let tr = "";
    $.each(data, function (ind, elem){
        tr += '<tr>'
        let idAtencion = 0
        $.each(elem, function (ind, dato){
            if(ind != 5)
            {
                tr += "<td>"+dato+"</td>";
            }
            if(ind == 5){
                idAtencion = dato
            }
        })
        tr += `<td align='left'>En atencion</td>`;
        tr += `<td align='left' title="Liberar Paciente">
                    <button  class="btn btn-link text-primary" onclick='liberarConsultaPaciente(${idAtencion}, 2)'>
                        <i class="fa fa-check" style="font-size:24px" aria-hidden="true"></i>
                    </button>
            </td>`;
        tr += "</tr>"; 
      });
     tabla.append(tr);
}