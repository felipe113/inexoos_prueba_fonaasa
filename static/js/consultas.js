var pacientePorConsulta = 0;
$( document ).ready(function() {

    getConsultas()
})

let getConsultas = () => {
    $.ajax({
        type:"POST",
        url: "/getConsultas",
        success:function(response){ 
            crearTablaConsultas(response)
        },
        error:function(response){
        }
    })
}

let crearTablaConsultas = (data) =>{
    let tbl = $("#tablaConsultas");
    if(data == ""){
        empty_row(tbl[0].id,'No hay datos en esta tabla')
        return
    }
    $("#tablaConsultas tbody").empty();
    let tabla = $("#tablaConsultas tbody");
    let tr = "";
    $.each(data, function (ind, elem){
        tr += '<tr>'
        $.each(elem, function (ind, dato){
            tr += "<td>"+dato+"</td>";
        })
        tr += "</tr>";
    })
    tabla.append(tr);
}

let librerarTodasConsultas = () => {
    $.ajax({
        type:"POST",
        url: "/librerarTodasConsultas",
        success:function(response){ 
            if(response == "OK")
            {
                messageAlert("Se han liberado todas las consultas del hospital.", 'success')
            }else{
                messageAlert("Ha ocurrido un error al liberar las consultas.", 'error')
            }
        },
        error:function(response){
        }
    })
}