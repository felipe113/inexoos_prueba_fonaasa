$( document ).ready(function() {
    $("#formularioMayorRiesgo").on('submit', function(event){
        event.preventDefault();
        $.ajax({
            type:"POST",
            url: "/getMayorRiesgo",
            data:{
                idHistoriaClinica : $("#idHistoriaClinica").val()
            },
            success:function(response){
                crearTablaMayorRiesgo(response)
            },
            error:function(response){
                messageAlert("Ha ocurrido un error al crear al paciente.", 'success')
            }
        })
    })
})

let crearTablaMayorRiesgo = (data) => {
    let tbl = $("#tablaMayorRiesgo");
    if(data == 0){
        empty_row(tbl[0].id,'No hay datos en esta tabla')
        return
    }
    
    $("#tablaMayorRiesgo tbody").empty();
    let tabla = $("#tablaMayorRiesgo tbody");
    let tr = "";
    $.each(data, function (ind, elem){
        tr += '<tr>'
        $.each(elem, function (ind, dato){
            tr += "<td>"+dato+"</td>";
        })
        tr += "</tr>"; 
      });
     tabla.append(tr);
}