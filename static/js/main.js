var pacientesCola = 0;

$(document).ready(function () {

    $(".cerrarModal").click(function () {
        $(".form-modal").modal('hide')
    });
});



let activarLiberar = () => {
    if (pacientesCola > 0) {
        $("#liberarConsulta").removeAttr('disabled');
    } else if (pacientesCola == 0) {
        $("#liberarConsulta").prop("disabled", true);
        //$("#liberarConsulta").attr('disabled');
    }
}

let messageAlert = (text, icon) => {
    let titulo = icon == 'error' ? '¡Ups ha ocurrido un error!' : '¡Proceso existoso!';
    Swal.fire({
        title: titulo,
        text: text,
        icon: icon,
        confirmButtonColor: '#3085e3',
        confirmButtonText: 'Aceptar'
    })
}



let empty_row = (id_table, msg) => {
    let tabla = $(`#${id_table}`);
    $(`#${id_table} tbody`).empty();
    let tr = "<tr>";
    tr += "<td class='text-center' colspan='12'>" + msg;
    tr += "</tr>";
    tabla.append(tr);
}


let liberarConsultaPaciente = (idAtencion, tipoConsulta) => {
    $.ajax({
        type: "POST",
        url: "/liberarPaciente",
        data: { 
            idAtencion: idAtencion
        },
        success: function (response) {
            if(response == "OK")
            {
                if (tipoConsulta === 1) {
                    getPacientesPediatria();
                } else if (tipoConsulta === 2) {
                    getPacientesCGI();
                } else {
                    getPacientesUrgencia();
                }
                messageAlert("Se ha Liberado al paciente.", "success")
            }
            
        },
        error: function (response) {
        }
    })
}

let reorganizarPacientes = () => {
    $.ajax({
        type: "POST",
        url: "/reorganizarPacientes",
        success: function (response) {
            
        },
        error: function (response) {
        }
    })
}

let getTipoConsulta = (idConsulta) => {
    switch(idConsulta) {
        case 1: return "Pediatria"
        case 2: return "CGI"
        case 3: return "Urgencia"
    }
}
