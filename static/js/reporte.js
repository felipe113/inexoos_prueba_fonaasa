$( document ).ready(function() {
    getConsultaMasAtenciones()
    getPacienteMasAnciano()
})

let getConsultaMasAtenciones = () =>
{
    $.ajax({
        type:"POST",
        url: "/getConsultaMasAtenciones",
        success:function(response){ 
            crearTablaConsultasMasAtenciones(response)
        },
        error:function(response){
        }
    })
}

let crearTablaConsultasMasAtenciones = (data) => {
    let tbl = $("#tablaConsultaMasAtenciones");
    if (data == "") {
        empty_row(tbl[0].id, 'No hay datos en esta tabla')
        return
    }

    $("#tablaConsultaMasAtenciones tbody").empty();
    let tabla = $("#tablaConsultaMasAtenciones tbody");
    let tr = "";
    $.each(data, function (ind, elem) {
        tr += '<tr>'
        $.each(elem, function (ind, dato) {
            tr += "<td>" + dato + "</td>";
        })
        tr += "</tr>";
    });
    tabla.append(tr);
}

let getPacienteMasAnciano = () =>
{
    $.ajax({
        type:"POST",
        url: "/getPacienteMasAnciano",
        success:function(response){ 
            crearTablaPacienteMasAnciano(response)
        },
        error:function(response){
        }
    })
}

let crearTablaPacienteMasAnciano = (data) => {
    let tbl = $("#tablaPacienteMasAnciano");
    if (data == "") {
        empty_row(tbl[0].id, 'No hay datos en esta tabla')
        return
    }

    $("#tablaPacienteMasAnciano tbody").empty();
    let tabla = $("#tablaPacienteMasAnciano tbody");
    let tr = "";
    tr += '<tr>'
    $.each(data, function ( ind, elem) {
        tr += "<td>" + elem + "</td>";
    });
    tr += "</tr>";
    tabla.append(tr);
}