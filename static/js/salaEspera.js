var es_fumador = 0;
var tiene_dieta = 0;
var tipoPaciente = 0;
$(document).ready(function () {
    getPacientesSalaEspera()
    
    $("#formularioNuevoPaciente").on('submit', function (event) {
        var tiempo_fumador = $("#tiempoFumador").val() == '' ? 0 : $("#tiempoFumador").val()
        var peso = $("#pesoPaciente").val() == '' ? 0 : $("#pesoPaciente").val()
        var estatura = $("#estaturaPaciente").val() == '' ? 0 : $("#estaturaPaciente").val()
        event.preventDefault();
        $.ajax({
            type: "POST",
            url: "/enviaDatos",
            data: {
                idHistoriaClinica: $("#idHistoriaClinica").val(),
                nombres: $("#nombrePaciente").val(),
                apellidos: $("#apellidoPaciente").val(),
                tipoPaciente: $("#listPacienteid").val(),
                edad: $("#edadPaciente").val(),
                peso: peso,
                estatura: estatura,
                es_fumador: $('input:radio[name=fumadorRadio]:checked').val(),
                tiempoFumador: tiempo_fumador,
                dieta: $('input:radio[name=dietaRadio]:checked').val()
            },
            success: function (response) {
                if(response[1] == 1)
                {
                    getPacientesSalaEspera()
                    messageAlert("El paciente se ha asignado a la consulta "+getTipoConsulta(response[0]), 'success')
                }
                else
                {
                    getPacientesSalaEspera()
                    messageAlert("Consulta "+getTipoConsulta(response[0])+" completa. Se agrega a la cola de espera.", 'success')
                }
                $(".form-modal").modal('hide')
                $('#formularioNuevoPaciente').trigger("reset");
                changePaciente()
            },
            error: function (response) {
                messageAlert("Ha ocurrido un error al crear al paciente.", 'success')
            }
        })
    })

    
})

$(".cerrarModal").click(function () {
    $('#formularioNuevoPaciente').trigger("reset");
    changePaciente()
});

let openModalNuevoPaciente =() =>{
    $('#modalFormPaciente').modal('show')
}

let getPacientesSalaEspera = () => {
    $.ajax({
        type: "POST",
        url: "/getPacientesSalaEspera",
        data: {},
        success: function (response) {
            crearTablaPacientesSalaEspera(response)
        },
        error: function (response) {
        }
    })
}


let getPacientesOptimizarSalaEspera = () => {
    $.ajax({
        type: "POST",
        url: "/getPacientesOptimizarSalaEspera",
        data: {},
        success: function (response) {
            crearTablaPacientesSalaEspera(response)
        },
        error: function (response) {
        }
    })
}

let crearTablaPacientesSalaEspera = (data) => {
    let tbl = $("#tablePacientesSalaEspera");
    if (data == "") {
        empty_row(tbl[0].id, 'No hay datos en esta tabla')
        return
    }

    $("#tablePacientesSalaEspera tbody").empty();
    let tabla = $("#tablePacientesSalaEspera tbody");
    let tr = "";
    $.each(data, function (ind, elem) {
        tr += '<tr>'
        let idAtencion = 0;
        let tipoConsulta = 0
        $.each(elem, function (ind, dato) {
            if (ind != 5) {
                if(ind != 6) {
                    if(ind != 7)
                    {
                        tr += "<td>" + dato + "</td>";
                    }
                }
            }
            if (ind == 5) {
                idAtencion = dato
            }
            if (ind == 7) {
                tipoConsulta = dato
            }
        })
        tr += `<td align='left'>
                    <button class="btn btn-link text-primary" title="Atender Paciente" onclick='atenderPaciente(${idAtencion}, ${tipoConsulta})'>
                        <i class="fa fa-check" style="font-size:24px" aria-hidden="true"></i>
                    </button>
            </td>`;
        tr += "</tr>";
    });
    tabla.append(tr);
}

let atenderPaciente = (idAtencion, tipoConsulta) => {
    $.ajax({
        type: "POST",
        url: "/atenderPaciente",
        data: {
            idAtencion : idAtencion,
            tipoConsulta : tipoConsulta
        },
        success: function (response) {
            if(response == "OK")
            {
                messageAlert("El paciente sera atendido en "+getTipoConsulta(tipoConsulta)+".", 'success')
                getPacientesSalaEspera()
            }else{
                messageAlert("La consulta de "+getTipoConsulta(tipoConsulta)+" esta al maximo de su capacidad.", 'error')
            }
        },
        error: function (response) {
        }
    })
}
let changePaciente = () => {
    let idTipoPaciente = $("#listPacienteid").val()
    if (idTipoPaciente == 2) {
        activaFumador();
    } else if (idTipoPaciente == 3) {
        activaAnciano()
    } else {
        $(".child-class").css("display", "block")
        $(".dieta-class").css("display", "none")
        $(".fumador-class").css("display", "none")
    }
}

let activaFumador = () => {
    $(".child-class").css("display", "none")
    $(".dieta-class").css("display", "none")
    $(".fumador-class").css("display", "block")
}

let activaAnciano = () => {
    $(".child-class").css("display", "none")
    $(".dieta-class").css("display", "block")
    $(".fumador-class").css("display", "none")
}

let desbloquearFumador = (event) => {
    if (event.target.value != 'NO') {
        es_fumador = 1;
        $(".tiempoFumador").removeAttr('disabled')
    } else {
        es_fumador = 0;
        $(".tiempoFumador").val("")
        $(".tiempoFumador").attr('disabled', true)
    }
}

let desbloquearDieta = (event) => {
    if (event.target.value != 'NO') {
        tiene_dieta = 1;
    } else {
        tiene_dieta = 0;
    }
}

let ordenAtencionNormal = () => {
    getPacientesSalaEspera()
}

let ordenAtencionOptimizada = () => {
    getPacientesOptimizarSalaEspera()
}