-- MySQL dump 10.13  Distrib 8.0.29, for Win64 (x86_64)
--
-- Host: localhost    Database: sistema_fonasa
-- ------------------------------------------------------
-- Server version	8.0.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `atenciones`
--

DROP TABLE IF EXISTS `atenciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `atenciones` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_paciente` int DEFAULT NULL,
  `id_consulta` int DEFAULT NULL,
  `estado_consulta` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=97 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `atenciones`
--

LOCK TABLES `atenciones` WRITE;
/*!40000 ALTER TABLE `atenciones` DISABLE KEYS */;
INSERT INTO `atenciones` VALUES (80,103,2,1),(81,104,2,1),(82,105,3,0),(83,106,3,1),(84,107,1,1),(85,108,2,1),(86,109,2,2),(87,110,2,2),(88,111,3,2),(89,112,2,2),(90,113,1,1),(91,114,1,1),(92,115,3,1),(93,116,3,2),(94,117,2,2),(95,118,2,2),(96,119,2,2);
/*!40000 ALTER TABLE `atenciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `consulta`
--

DROP TABLE IF EXISTS `consulta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `consulta` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_hospital` int NOT NULL DEFAULT '0',
  `nombre` varchar(50) NOT NULL DEFAULT '0',
  `cantidad_pacientes_atender` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `consulta`
--

LOCK TABLES `consulta` WRITE;
/*!40000 ALTER TABLE `consulta` DISABLE KEYS */;
INSERT INTO `consulta` VALUES (1,1,'Pediatría',3),(2,1,'CGI',3),(3,1,'Urgencia',2);
/*!40000 ALTER TABLE `consulta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `especialistas`
--

DROP TABLE IF EXISTS `especialistas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `especialistas` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_consulta` int NOT NULL DEFAULT '0',
  `nombre` varchar(100) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `especialistas`
--

LOCK TABLES `especialistas` WRITE;
/*!40000 ALTER TABLE `especialistas` DISABLE KEYS */;
INSERT INTO `especialistas` VALUES (1,1,'Priscila Cortes'),(2,2,'Carlos Apablaza'),(3,3,'Mabel Herrera');
/*!40000 ALTER TABLE `especialistas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estados`
--

DROP TABLE IF EXISTS `estados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `estados` (
  `id` int NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estados`
--

LOCK TABLES `estados` WRITE;
/*!40000 ALTER TABLE `estados` DISABLE KEYS */;
INSERT INTO `estados` VALUES (0,'Atendido'),(1,'En atencion'),(2,'Sala de espera');
/*!40000 ALTER TABLE `estados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hospital`
--

DROP TABLE IF EXISTS `hospital`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hospital` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hospital`
--

LOCK TABLES `hospital` WRITE;
/*!40000 ALTER TABLE `hospital` DISABLE KEYS */;
INSERT INTO `hospital` VALUES (1,'Hospital Fonasa');
/*!40000 ALTER TABLE `hospital` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pacientes`
--

DROP TABLE IF EXISTS `pacientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pacientes` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nombres` varchar(100) DEFAULT NULL,
  `apellidos` varchar(100) DEFAULT NULL,
  `edad` int DEFAULT NULL,
  `tipo_paciente` int DEFAULT NULL,
  `id_consulta` int DEFAULT NULL,
  `id_historia_clinica` int DEFAULT NULL,
  `id_hospital` int DEFAULT NULL,
  `fecha_ingreso` datetime DEFAULT NULL,
  `riesgo` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=120 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pacientes`
--

LOCK TABLES `pacientes` WRITE;
/*!40000 ALTER TABLE `pacientes` DISABLE KEYS */;
INSERT INTO `pacientes` VALUES (103,'Felipe','Apablaza',32,2,2,11223344,1,'2022-07-15 07:03:10',1),(104,'Priscila','Cortes',33,2,2,22113344,1,'2022-07-15 07:03:44',1),(105,'Mabel','Herrera',65,3,3,33112244,1,'2022-07-15 07:04:29',9),(106,'Carlos','Apablaza',69,3,3,44112233,1,'2022-07-15 07:04:52',9),(107,'Alejandra','Apablaza',4,1,1,12112233,1,'2022-07-15 07:05:51',0),(108,'Carla','Apablaza',30,2,2,13112233,1,'2022-07-15 07:06:10',1),(109,'Ariel','Bollo',35,2,2,13223344,1,'2022-07-15 07:06:57',1),(110,'Priscila','Alvarez',29,2,2,14223344,1,'2022-07-15 07:08:07',1),(111,'Juan','Herrera',75,3,3,21113344,1,'2022-07-15 07:09:04',9),(112,'Jurek','Hernandez',32,2,2,22113344,1,'2022-07-15 07:17:10',1),(113,'Francisca','Hernandez',12,1,1,23113344,1,'2022-07-15 07:17:52',-12),(114,'Nicole','Hernandez',12,1,1,23441122,1,'2022-07-15 07:18:36',-10),(115,'Gustavo','Cortes',70,3,3,3122334,1,'2022-07-15 07:19:04',9),(116,'Patricia','Toledo',65,3,3,3222334,1,'2022-07-15 07:19:27',9),(117,'Gustavo','Cortes Toledo',23,2,2,3322334,1,'2022-07-15 07:20:06',1),(118,'Barbara','Cortes Toledo',30,2,2,3422334,1,'2022-07-15 07:20:26',1),(119,'Thania','Cortes',39,2,2,31223344,1,'2022-07-15 07:28:54',1);
/*!40000 ALTER TABLE `pacientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `panciano`
--

DROP TABLE IF EXISTS `panciano`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `panciano` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_paciente` int DEFAULT NULL,
  `tiene_dieta` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `panciano`
--

LOCK TABLES `panciano` WRITE;
/*!40000 ALTER TABLE `panciano` DISABLE KEYS */;
INSERT INTO `panciano` VALUES (2,105,1),(3,106,1),(4,111,1),(5,115,0),(6,116,1);
/*!40000 ALTER TABLE `panciano` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pjoven`
--

DROP TABLE IF EXISTS `pjoven`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pjoven` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_paciente` int DEFAULT NULL,
  `es_fumador` int DEFAULT NULL,
  `tiempo_fumador` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pjoven`
--

LOCK TABLES `pjoven` WRITE;
/*!40000 ALTER TABLE `pjoven` DISABLE KEYS */;
INSERT INTO `pjoven` VALUES (6,103,0,0),(7,104,0,0),(8,108,1,2),(9,109,0,0),(10,110,0,0),(11,112,0,0),(12,117,1,1),(13,118,0,0),(14,119,1,5);
/*!40000 ALTER TABLE `pjoven` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pninno`
--

DROP TABLE IF EXISTS `pninno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pninno` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_paciente` int DEFAULT NULL,
  `peso` int DEFAULT NULL,
  `estatura` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pninno`
--

LOCK TABLES `pninno` WRITE;
/*!40000 ALTER TABLE `pninno` DISABLE KEYS */;
INSERT INTO `pninno` VALUES (3,107,60,60),(4,113,50,150),(5,114,65,150);
/*!40000 ALTER TABLE `pninno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_paciente`
--

DROP TABLE IF EXISTS `tipo_paciente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tipo_paciente` (
  `id` int NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_paciente`
--

LOCK TABLES `tipo_paciente` WRITE;
/*!40000 ALTER TABLE `tipo_paciente` DISABLE KEYS */;
INSERT INTO `tipo_paciente` VALUES (1,'Niño'),(2,'Adulto Joven'),(3,'Anciano');
/*!40000 ALTER TABLE `tipo_paciente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` char(102) DEFAULT NULL,
  `fullName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (3,'administrador','administrador','Administrador');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-07-15  7:33:11
