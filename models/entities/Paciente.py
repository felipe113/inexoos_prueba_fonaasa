class Pacientes():

    def __init__(self, id, nombres, apellidos, edad, tipo_paciente, id_consulta, id_historia_clinica, riesgo, id_hospital = 1) -> None:
        self.id = id
        self.nombres = nombres
        self.apellidos = apellidos
        self.edad = edad
        self.tipo_paciente = tipo_paciente
        self.id_consulta = id_consulta
        self.id_historia_clinica = id_historia_clinica
        self.id_hospital = id_hospital
        self.riesgo = riesgo

    def calculaPrioridad(edad, esFumador, tiempoFumador, dieta, peso, estatura):
        if edad >= 0 and edad <= 15:
            if edad >= 0 and edad <= 5:
                prioridad = int(peso) - int(estatura) + 3
            if edad >= 6 and edad <= 12:
                prioridad = int(peso) - int(estatura) + 2
            if edad >= 13 and edad <= 12:
                prioridad = int(peso) - int(estatura) + 1
        if edad >= 16 and edad <= 40:
            prioridad = 2
            if int(esFumador) == 1:
                prioridad = int(tiempoFumador)/4 + 2
        if edad >= 41:   
            prioridad = edad/30 +3
            if dieta == 1 and edad >= 60 and edad <= 100:
                prioridad = edad/20 + 4

        return prioridad

    def calculaRiesgo(prioridad, edad):
        riesgo = (edad * prioridad) / 100
        if edad >= 41:
            riesgo = (edad * prioridad)/100 + 5.3
        return riesgo

    