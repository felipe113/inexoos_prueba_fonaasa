class Consulta():

    def __init__(self, id, id_hospital, nombre, estado, cantidad_pacientes_atender) -> None:
        self.id = id
        self.id_hospital = id_hospital
        self.nombre = nombre
        self.estado = estado
        self.cantidad_pacientes_atender = cantidad_pacientes_atender

    """1: pediatria  2: CGI  3: urgencia"""
    def evaluarTipoDeConsultaPorPaciente(edad, prioridad):
        if prioridad > 4:
            tipoDeconsulta = 3
            return tipoDeconsulta
        if edad <= 15 and prioridad <= 4:
            tipoDeconsulta = 1
            return tipoDeconsulta
        tipoDeconsulta = 2
        return tipoDeconsulta