from .entities.Consulta import Consulta


class ModelConsulta():

    def getPacientesPorConsulta(db, consulta):
        try:
            cursor = db.connection.cursor()   
            sql = """SELECT cantidad_pacientes_atender FROM consulta WHERE id = {}""".format(consulta)
            cursor.execute(sql)
            row = cursor.fetchone()
            return row[0]
        except Exception as ex:
            raise Exception(ex)  

    def getConsultas(db):
        try:
            cursor = db.connection.cursor()   
            sql = """SELECT c.id, h.nombre AS hospital, c.nombre as consulta, e.nombre, c.cantidad_pacientes_atender
                        FROM consulta c
                        INNER JOIN especialistas e  ON e.id_consulta = c.id
                        INNER JOIN hospital h ON h.id = c.id_hospital"""
            cursor.execute(sql)
            row = cursor.fetchall()
            return row
        except Exception as ex:
            raise Exception(ex)


    def getMayorRiesgo(db, idHistoarialClinico):
        try:
            cursor = db.connection.cursor()   
            sql = f"""SELECT riesgo FROM pacientes WHERE id_historia_clinica = {idHistoarialClinico}"""
            result = cursor.execute(sql)
            if result != 0:
                row = cursor.fetchone()
                sql = f"""SELECT p.nombres, p.apellidos, p.id_historia_clinica, p.riesgo FROM pacientes p 
                LEFT JOIN atenciones a ON a.id_paciente = p.id
                WHERE p.riesgo > {row[0]} AND p.riesgo IS NOT NULL AND a.estado_consulta != 0 ORDER BY p.riesgo ASC"""
                cursor.execute(sql)
                result = cursor.fetchall()
                return result
            return 0
        except Exception as ex:
            raise Exception(ex)

    def getCantidadMaximaParaAtender(db , tipoConsulta):
        try:
            cursor = db.connection.cursor()   
            sql = f"""SELECT cantidad_pacientes_atender FROM consulta WHERE id = {tipoConsulta}"""
            cursor.execute(sql)
            row = cursor.fetchone()
            return row[0]
        except Exception as ex:
            raise Exception(ex)

    def liberarPaciente(db, idAtencion):
        try:
            cursor = db.connection.cursor()   
            sql = f"""UPDATE atenciones
                        SET estado_consulta = 0
                        WHERE id = {idAtencion}"""
            result = cursor.execute(sql)
            return result
        except Exception as ex:
            raise Exception(ex)

    def atenderPaciente(db, idAtencion):
        try:
            cursor = db.connection.cursor()   
            sql = f"""UPDATE atenciones
                        SET estado_consulta = 1
                        WHERE id = {idAtencion}"""
            result = cursor.execute(sql)
            return result
        except Exception as ex:
            raise Exception(ex)

    def getPacientesActualesPorConsulta(db, tipoConsulta):
        try:
            cursor = db.connection.cursor()   
            sql = f"""SELECT * FROM atenciones WHERE id_consulta = {tipoConsulta} AND estado_consulta = 1"""
            result = cursor.execute(sql)
            return result
        except Exception as ex:
            raise Exception(ex)
    
    def liberarTodasConsultas(db):
        try:
            cursor = db.connection.cursor()   
            sql = """UPDATE atenciones
                        SET estado_consulta = 0
                        WHERE estado_consulta = 1"""
            result = cursor.execute(sql)
            return result
        except Exception as ex:
            raise Exception(ex)

    def getTotalConsultas(db):
        try:
            cursor = db.connection.cursor()   
            sql = """SELECT id FROM consulta"""
            result = cursor.execute(sql)
            return result
        except Exception as ex:
            raise Exception(ex)

    def atenderGrupoPacientes(db, maximoPacientes, idConsulta):
        try:
            cursor = db.connection.cursor()   
            sql = f"""UPDATE atenciones a SET a.estado_consulta = 1 WHERE id_consulta = {idConsulta} AND estado_consulta = 2
                    ORDER BY id ASC LIMIT {maximoPacientes}"""
            result = cursor.execute(sql)
            return result
        except Exception as ex:
            raise Exception(ex)

    def getConsultaMasAtenciones(db):
        try:
            cursor = db.connection.cursor()   
            sql = """SELECT (SELECT COUNT(*) FROM atenciones WHERE id_consulta = 1 AND estado_consulta = 0) AS pediatria,
             (SELECT COUNT(*) FROM atenciones WHERE id_consulta = 2 AND estado_consulta = 0) AS cgi, 
             (SELECT COUNT(*) FROM atenciones WHERE id_consulta = 3 AND estado_consulta = 0) AS urgencia 
             FROM atenciones limit 1"""
            cursor.execute(sql)
            row = cursor.fetchall()
            return row
        except Exception as ex:
            raise Exception(ex)

    def getPacienteMasAnciano(db):
        try:
            cursor = db.connection.cursor()   
            sql = """SELECT p.nombres, p.apellidos, p.edad, p.id_historia_clinica FROM atenciones a
                        LEFT JOIN pacientes p ON p.id = a.id_paciente
                        WHERE a.estado_consulta = 2
                        ORDER BY p.edad DESC"""
            cursor.execute(sql)
            row = cursor.fetchone()
            return row
        except Exception as ex:
            raise Exception(ex)

    