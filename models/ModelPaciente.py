from .entities.Paciente import Pacientes


class ModelPacientes():

    def save(db, paciente):
        try:
            cursor = db.connection.cursor()
            sql = f""" INSERT INTO pacientes (nombres, apellidos, edad, tipo_paciente, id_consulta, id_historia_clinica, id_hospital, fecha_ingreso, riesgo ) 
                       VALUES('{paciente.nombres}','{paciente.apellidos}', {paciente.edad}, {paciente.tipo_paciente}, {paciente.id_consulta}, {paciente.id_historia_clinica}, {paciente.id_hospital}, NOW(), {paciente.riesgo})"""
            cursor.execute(sql)
            record_last = cursor.lastrowid
            return record_last

        except Exception as ex:
            raise Exception(ex)


    def getPacientesUrgencia(db):
        try:
            cursor = db.connection.cursor()   
            sql = """SELECT p.id, p.nombres, p.apellidos, p.id_historia_clinica, e.nombre, a.id FROM pacientes p 
                    LEFT JOIN atenciones a ON a.id_paciente = p.id 
                    LEFT JOIN especialistas e ON a.id_consulta = e.id_consulta 
                    WHERE a.estado_consulta = 1 AND a.id_consulta = 3 """ 
           
            cursor.execute(sql)
            row = cursor.fetchall()
            if row != None:
                return row
            else:
                return None
        except Exception as ex:
            raise Exception(ex)

    def getPacientesPediatria(db):
        try:
            cursor = db.connection.cursor()   
            sql = """ SELECT p.id, p.nombres, p.apellidos, p.id_historia_clinica, e.nombre, a.id FROM pacientes p 
                    LEFT JOIN atenciones a ON a.id_paciente = p.id 
                    LEFT JOIN especialistas e ON a.id_consulta = e.id_consulta 
                    WHERE a.estado_consulta = 1 AND a.id_consulta = 1 """ 
           
            cursor.execute(sql)
            row = cursor.fetchall()
            if row != None:
                return row
            else:
                return None
        except Exception as ex:
            raise Exception(ex)      

    def getPacientesCGI(db):
            try:
                cursor = db.connection.cursor()   
                sql = """ SELECT p.id, p.nombres, p.apellidos, p.id_historia_clinica, e.nombre, a.id FROM pacientes p 
                    LEFT JOIN atenciones a ON a.id_paciente = p.id 
                    LEFT JOIN especialistas e ON a.id_consulta = e.id_consulta 
                    WHERE a.estado_consulta = 1 AND a.id_consulta = 2 """ 
                cursor.execute(sql)
                row = cursor.fetchall()
                if row != None:
                    return row
                else:
                    return None
            except Exception as ex:
                raise Exception(ex)      
  

    def getPacientesSalaEspera(db):
        try:
            cursor = db.connection.cursor()   
            sql = """SELECT p.id, p.nombres, p.apellidos,p.edad , p.id_historia_clinica, a.id,
                    CASE WHEN (p.edad BETWEEN 1 AND 5) THEN TRUNCATE(( pn.peso - pn.estatura + 3), 0) 
                        ELSE CASE WHEN (p.edad BETWEEN 6 AND 12) THEN TRUNCATE(( pn.peso - pn.estatura + 2), 0)
                            ELSE CASE WHEN (p.edad BETWEEN 13 AND 15) THEN TRUNCATE(( pn.peso - pn.estatura + 1), 0)
                                ELSE CASE WHEN (p.tipo_paciente = 3 AND pa.tiene_dieta = 1 AND p.edad BETWEEN 60 AND 100 ) THEN TRUNCATE((p.edad/20) + 4 ,4)
                                    ELSE CASE WHEN   (p.tipo_paciente = 3 AND pa.tiene_dieta = 0) THEN TRUNCATE((p.edad/30 + 3) , 4)
                                        ELSE CASE WHEN (p.tipo_paciente = 2 AND pj.es_fumador = 1) THEN TRUNCATE((pj.tiempo_fumador/4 + 2),0)
                                            ELSE 2 END END END END END END 
                    AS prioridad, a.id_consulta, e.nombre, c.nombre
                    FROM pacientes p 
                    LEFT JOIN atenciones a ON a.id_paciente = p.id
                    LEFT JOIN consulta c ON c.id = a.id_consulta
                    LEFT JOIN especialistas e ON e.id_consulta = a.id_consulta
                    LEFT JOIN pninno pn ON pn.id_paciente = p.id
                    LEFT JOIN pjoven pj ON pj.id_paciente = p.id
                    LEFT JOIN panciano pa ON pa.id_paciente = p.id
                    WHERE a.estado_consulta = 2
                    ORDER BY p.fecha_ingreso ASC, prioridad ASC"""
            cursor.execute(sql)
            row = cursor.fetchall()
            if row != None:
                return row
            else:
                return None
        except Exception as ex:
            raise Exception(ex)  

    def getPacientesOptimizarSalaEspera(db):
        try:
            cursor = db.connection.cursor()   
            sql = """ SELECT p.id, p.nombres, p.apellidos,p.edad , p.id_historia_clinica, a.id,
                CASE WHEN (p.edad BETWEEN 1 AND 5) THEN TRUNCATE(( pn.peso - pn.estatura + 3), 0) 
                    ELSE CASE WHEN (p.edad BETWEEN 6 AND 12) THEN TRUNCATE(( pn.peso - pn.estatura + 2), 0)
                        ELSE CASE WHEN (p.edad BETWEEN 13 AND 15) THEN TRUNCATE(( pn.peso - pn.estatura + 1), 0)
                            ELSE CASE WHEN (p.tipo_paciente = 3 AND pa.tiene_dieta = 1 AND p.edad BETWEEN 60 AND 100 ) THEN TRUNCATE((p.edad/20) + 4 ,4)
                                ELSE CASE WHEN   (p.tipo_paciente = 3 AND pa.tiene_dieta = 0) THEN TRUNCATE((p.edad/30 + 3) , 4)
                                    ELSE CASE WHEN (p.tipo_paciente = 2 AND pj.es_fumador = 1) THEN TRUNCATE((pj.tiempo_fumador/4 + 2),0)
                                        ELSE 2 END END END END END END 
                AS prioridad, a.id_consulta, e.nombre, c.nombre
                FROM pacientes p 
                LEFT JOIN atenciones a ON a.id_paciente = p.id
                LEFT JOIN consulta c ON c.id = a.id_consulta
                LEFT JOIN especialistas e ON e.id_consulta = a.id_consulta
                LEFT JOIN pninno pn ON pn.id_paciente = p.id
                LEFT JOIN pjoven pj ON pj.id_paciente = p.id
                LEFT JOIN panciano pa ON pa.id_paciente = p.id
                WHERE a.estado_consulta = 2
                ORDER BY p.edad DESC, p.riesgo DESC """ 
            cursor.execute(sql)
            row = cursor.fetchall()
            if row != None:
                return row
            else:
                return None
        except Exception as ex:
            raise Exception(ex)