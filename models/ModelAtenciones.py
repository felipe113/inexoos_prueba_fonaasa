from .entities.Atenciones import Atenciones


class ModelAtenciones():

    def save(db, atencion):
        try:
            cursor = db.connection.cursor()   
            sql = f"""INSERT INTO atenciones(id_paciente, id_consulta, estado_consulta) VALUES ({ atencion.id_paciente }, { atencion.id_consulta }, { atencion.estado_consulta})"""
            cursor.execute(sql)
            record_last = cursor.lastrowid
            return record_last
        except Exception as ex:
            raise Exception(ex)  

    def getTotalAtencionesPorConsulta(db, tipoDeConsulta):
        try:
            cursor = db.connection.cursor()   
            sql = f"""SELECT a.id FROM atenciones a
                    LEFT JOIN consulta c ON c.id = a.id_consulta
                    WHERE estado_consulta = 1 AND id_consulta = { tipoDeConsulta }"""
            result = cursor.execute(sql)
            return result
        except Exception as ex:
            raise Exception(ex) 

    def getAtencionPorId(db, idAtencion):
        try:
            cursor = db.connection.cursor()   
            sql = f"""SELECT a.id_consulta, a.estado_consulta FROM atenciones a
                    WHERE a.id = { idAtencion }"""
            cursor.execute(sql)
            result = cursor.fetchone()
            return result
        except Exception as ex:
            raise Exception(ex) 
    
