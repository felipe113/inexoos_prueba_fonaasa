from unittest import result
from .entities.PacientesDetalles import PacientesDetalles


class ModelPacientesDetalles():
    def setPNinno(db, paciente):
        try:
            cursor = db.connection.cursor()
            sql = f""" INSERT INTO pninno (id_paciente, peso, estatura) 
                       VALUES({paciente.id_paciente},{paciente.peso}, {paciente.estatura})"""
            cursor.execute(sql)
            record_last = cursor.lastrowid
            return record_last
            
        except Exception as ex:
            raise Exception(ex)

    def setPJoven(db, paciente):
        try:
            cursor = db.connection.cursor()
            sql = f""" INSERT INTO pjoven (id_paciente, es_fumador, tiempo_fumador) 
                       VALUES({paciente.id_paciente},{paciente.es_fumador}, {paciente.tiempo_fumador})"""
            cursor.execute(sql)
            record_last = cursor.lastrowid
            return record_last
            
        except Exception as ex:
            raise Exception(ex)

    def setPAnciano(db, paciente):
        try:
            cursor = db.connection.cursor()
            sql = f""" INSERT INTO panciano (id_paciente, tiene_dieta) 
                       VALUES({paciente.id_paciente},{paciente.tiene_dieta})"""
            cursor.execute(sql)
            record_last = cursor.lastrowid
            return record_last
            
        except Exception as ex:
            raise Exception(ex)