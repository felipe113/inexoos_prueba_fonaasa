from flask import Flask, jsonify, render_template, request, redirect, url_for, flash
from flask_mysqldb import MySQL
from flask_login import LoginManager, login_user, logout_user, login_required

from config import config, constants
from models.ModelAtenciones import ModelAtenciones

# Models:
from models.ModelUser import ModelUser
from models.ModelPaciente import ModelPacientes
from models.ModelPacientesDetalles import ModelPacientesDetalles
from models.ModelConsulta import ModelConsulta
from models.entities.Atenciones import Atenciones

# Entities:
from models.entities.User import User
from models.entities.Paciente import Pacientes
from models.entities.PacientesDetalles import PacientesDetalles
from models.entities.Consulta import Consulta
from models.entities.Atenciones import Atenciones

app = Flask(__name__)

db = MySQL(app)
login_manager_app = LoginManager(app)


@login_manager_app.user_loader
def load_user(id):
    return ModelUser.get_by_id(db, id)


@app.route('/')
def index():
    return redirect(url_for('login'))


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        user = User(0, request.form['username'], request.form['password'])
        logged_user = ModelUser.login(db, user)
        if logged_user != None:
            if logged_user.password:
                login_user(logged_user)
                return redirect(url_for('salaDeEspera'))
            else:
                flash("Invalid password...")
                return render_template('auth/login.html')
        else:
            flash("User not found...")
            return render_template('auth/login.html')
    else:
        return render_template('auth/login.html')


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('login'))

@app.route('/urgencia')
@login_required
def urgencia():
    return render_template('paginas/urgencia.html')

@app.route('/pediatria')
@login_required
def pediatria():
    return render_template('paginas/pediatria.html')

@app.route('/cgi')
@login_required
def cgi():
    return render_template('paginas/cgi.html')

@app.route('/salaDeEspera')
@login_required
def salaDeEspera():
    return render_template('paginas/salaDeEspera.html')

@app.route('/consultas')
@login_required
def consultas():
    return render_template('paginas/consultas.html')

@app.route('/construccion')
@login_required
def construccion():
    return render_template('paginas/construccion.html')

@app.route('/mayorRiesgo')
@login_required
def mayorRiesgo():
    return render_template('paginas/mayorRiesgo.html')

@app.route('/reporte')
@login_required
def reporte():
    return render_template('paginas/reporte.html')

def status_401(error):
    return redirect(url_for('login'))


def status_404(error):
    return render_template('paginas/urgencia.html')


@app.route('/enviaDatos', methods=['POST'])
def enviaDatos():
    
    prioridad = Pacientes.calculaPrioridad(int(request.form['edad']), request.form['es_fumador'], request.form['tiempoFumador'], request.form['dieta'], request.form['peso'] , request.form['estatura'])
    riesgo = Pacientes.calculaRiesgo(prioridad, int(request.form['edad']))
    paciente = Pacientes(0, request.form['nombres'], request.form['apellidos'],request.form['edad'],request.form['tipoPaciente'],request.form['tipoPaciente'],request.form['idHistoriaClinica'], riesgo)
    
    
    tipo_consulta = Consulta.evaluarTipoDeConsultaPorPaciente(int(request.form['edad']), prioridad)
    maximo_consulta = ModelConsulta.getCantidadMaximaParaAtender(db, tipo_consulta)
    atenciones_actuales = ModelAtenciones.getTotalAtencionesPorConsulta(db, tipo_consulta)
    insertPaciente = ModelPacientes.save(db, paciente)
    
    if atenciones_actuales < maximo_consulta:
        atencion = Atenciones(0, insertPaciente, tipo_consulta, 1)
    else:
        atencion = Atenciones(0, insertPaciente, tipo_consulta, 2)
    idAtencion = ModelAtenciones.save(db, atencion)
    if insertPaciente > 0:
        pacienteDetalles = PacientesDetalles(0, request.form['es_fumador'], request.form['tiempoFumador'], request.form['dieta'],request.form['peso'],request.form['estatura'],insertPaciente)
        if int(request.form['tipoPaciente']) == 1:
            ModelPacientesDetalles.setPNinno(db,pacienteDetalles)
        elif int(request.form['tipoPaciente']) == 2:
            ModelPacientesDetalles.setPJoven(db,pacienteDetalles)
        elif int(request.form['tipoPaciente']) == 3:
            ModelPacientesDetalles.setPAnciano(db,pacienteDetalles)
        atencionFinal = ModelAtenciones.getAtencionPorId(db, idAtencion)
        return jsonify((atencionFinal))  
    else:
        return "Error"

@app.route('/getPacientesSalaEspera', methods=['POST'])    
def getPacientesSalaEspera():
    paciente = ModelPacientes.getPacientesSalaEspera(db)
    return jsonify((paciente))  

@app.route('/getPacientesUrgencia', methods=['POST'])
def getPacientesUrgencia():
   urgencias = ModelPacientes.getPacientesUrgencia(db)
   return jsonify((urgencias)) 

@app.route('/getPacientesPediatria', methods=['POST'])
def getPacientesPediatria():
    pacientes = ModelPacientes.getPacientesPediatria(db)
    return jsonify((pacientes)) 

@app.route('/getPacientesCGI', methods=['POST'])
def getPacientesCGI():
    pacientes = ModelPacientes.getPacientesCGI(db)
    return jsonify((pacientes))   

@app.route('/getPacientesPorConsulta', methods=['POST'])
def getPacientesPorConsulta():
    consulta = ModelConsulta.getPacientesPorConsulta(db,request.form['idConsulta'])
    return jsonify((consulta))


@app.route('/getConsultas', methods=['POST'])
def getConsultas():
    consultas = ModelConsulta.getConsultas(db)
    return jsonify((consultas))

@app.route('/liberarPaciente', methods=['POST'])
def liberarPaciente():
    idAtencion = request.form['idAtencion']
    ModelConsulta.liberarPaciente(db, idAtencion)
    return jsonify("OK")


@app.route('/atenderPaciente', methods=['POST'])
def atenderPaciente():
    idAtencion = request.form['idAtencion']
    tipoConsulta = request.form['tipoConsulta']
    maximoAtenciones = ModelConsulta.getCantidadMaximaParaAtender(db, tipoConsulta)
    atencionesActuales = ModelConsulta.getPacientesActualesPorConsulta(db, tipoConsulta)
    if atencionesActuales < maximoAtenciones:
        ModelConsulta.atenderPaciente(db, idAtencion)
        return jsonify("OK")
    return jsonify("ERROR")


@app.route('/librerarTodasConsultas', methods=['POST'])
def librerarTodasConsultas():
    ModelConsulta.liberarTodasConsultas(db)
    totalConsultas = ModelConsulta.getTotalConsultas(db)
    for i in range(totalConsultas):
        maximoPacientes = ModelConsulta.getCantidadMaximaParaAtender(db, i+1)
        ModelConsulta.atenderGrupoPacientes(db, maximoPacientes, i+1)
    return jsonify("OK")


@app.route('/getConsultaMasAtenciones', methods=['POST'])
def getConsultaMasAtenciones():
    result = ModelConsulta.getConsultaMasAtenciones(db)
    return jsonify((result))


@app.route('/getPacienteMasAnciano', methods=['POST'])
def getPacienteMasAnciano():
    result = ModelConsulta.getPacienteMasAnciano(db)
    return jsonify((result))


@app.route('/reorganizarPacientes', methods=['POST'])
def reorganizarPacientes():
    ModelConsulta.reorganizarPacientes(db)
    return jsonify("OK")


@app.route('/getMayorRiesgo', methods=['POST'])
def getMayorRiesgo():
    idHistorialClinico = request.form['idHistoriaClinica']
    result = ModelConsulta.getMayorRiesgo(db, idHistorialClinico)
    return jsonify((result))

@app.route('/getPacientesOptimizarSalaEspera', methods=['POST'])
def getPacientesOptimizarSalaEspera():
    paciente = ModelPacientes.getPacientesOptimizarSalaEspera(db)
    return jsonify((paciente))  


if __name__ == '__main__':
    app.config.from_object(config['development'])
    app.register_error_handler(401, status_401)
    app.register_error_handler(404, status_404)
    app.run()